#!/bin/bash -xv

# Задаем текущую дииректорию
currd=`printf $PWD`;

# Функция расчета типа треугольника
tip_treyg() {
	# Сортируем значения по возрастанию
	sortt=`echo "$a $b $c " | tr ' ' '\n' | sort | tr '\n' ';' | sed "s/;//"`;

	# Задаем стороны
	a=`echo "$sortt" | awk -F ';' {'print $1'}`;
	b=`echo "$sortt" | awk -F ';' {'print $2'}`;
	c=`echo "$sortt" | awk -F ';' {'print $3'}`;

	# Выдаем результат
	if [ $a -eq $b -a $b -eq $c ]; then
		g="равносторонний";
		printf "$g";
	elif [ $a -ne $b -a $b -eq $c ]; then
		g="равнобедренный";
		printf "$g";
	else
		g="разносторонний";
		printf "$g";
	fi;
};

# Запускаем цикл проверок
proverka() {

	# Проверяем, что переменные не пустые
	if [ -z "$a" -o -z "$b" -o -z "$c" ]; then
	    printf "Что-то пошло не так" >&2;
	    exit 1;
	fi;

	# Проверяем, что все числа - целые
	chislo=`echo "$a$b$c" | tr -d '[0-9]'`;
	if [ ! -z "$chislo" ]; then
	    printf "Что-то пошло не так" >&2;
	    exit 1;
	fi;

	# Проверяем, что каждое из значений не превышает максимально допустимое значение в bourne shell
	# Для начала ищем числа, в которых кол-во символов = 20
	
	# Функция проверки максимально допустимого значения
	max_values() {
		for i in $1 $2 $3
		do
			# Считаем кол-во символов в строке
			simv=`echo $i | wc -c`;
			# Если сиволов = 20
			if [ "$simv" -eq 20 ]; then
				# Проверяем, что первые 18 символов <= первых 18 символов максимально допустимого значения в bourne shell
				# Задаем максимальное значение первых 18 символов
				maxvalue="92233720368547758";
				# Задаем текущее значение  первых 18 символов
				currvalue=`echo $i | rev | cut -c 3- | rev`
				if [ $currvalue -gt $maxvalue ]; then
	    			printf "Что-то пошло не так" >&2;
	    			exit 1;
				else
					# Проверяем, что последние 2 символа <= последних 2 символов максимально допустимого значения в bourne shell
					# Задаем максимальное значение последних 2 символов
					lmaxvalue="7"
					# Задаем текущее значение последних 2 символов
					lcurrvalue=`echo $i | cut -c 18-`
					if [ $lmaxvalue -lt $lcurrvalue ]; then
	    				printf "Что-то пошло не так" >&2;
	    				exit 1;				
					fi;	
				fi;
			# Если сиволов > 20
			elif [ "$simv" -gt 20 ]; then
	    		printf "Что-то пошло не так" >&2;
	    		exit 1;	
			fi;
		done;	
	};
	
	# Проверяем, что числа не больше предельного значения в bash
	max_values $a $b $c;

	# Если одно из значений меньше, либо равно 0 - завершаем выполнение  92233720368547704
	if [ $a -le 0 -o $b -le 0 -o $c -le 0 ]; then
		printf "Что-то пошло не так" >&2;
		exit 1;
	fi;
	
	# Проверяем, что сумма любых 2-ух сторон меньше максимально допустимого в bourne shell
	fsumm=`echo "$a+$b" | bc`
	ssumm=`echo "$b+$c" | bc`
	tsumm=`echo "$a+$c" | bc`
	max_values $fsumm $ssumm $tsumm;
	
	# Проверяем, что сумма двух любых сторон больше третьей стороны
	if [ $fsumm -le $c -o $ssumm -le $a -o $tsumm -le $b ]; then
		printf "Что-то пошло не так" >&2;
		exit 1;	
	fi;

	# Запускаем расчет треугольника
	tip_treyg;
};

# Запускаем юнит-тесты
# Проверяем отработку условий на валидных значениях (без проверки на валидность)
# В данном тесте мы сравниваем результат выполнения с эталонным (тестирование стека функций initialization - tip_treyg)
unit_test_sf() {

	# Удаляем файл с результатами предыдущего выполнения
	rm $currd/tmp/result_sf.lst;
	# Начинаем цикл построчного чтения
	cat $currd/var/init_sf.lst | while read line
	do
		# Згоняем параметры из файла в цикл
		for param in $line
		do
			# Парсим строку
			a=`printf "$param" | awk -F ";" {'print $1'}`;
			b=`printf "$param" | awk -F ";" {'print $2'}`;
			c=`printf "$param" | awk -F ";" {'print $3'}`;
			tip=`printf "$param" | awk -F ";" {'print $4'}`;
			# Запускаем проверку
			result=`tip_treyg $a $b $c`;
			if [ "$result" != "$tip" ]; then
				printf "$param;FAIL\n" >> $currd/tmp/result_sf.lst;
			else
				printf "$param;SUCCESS\n" >> $currd/tmp/result_sf.lst;
			fi;
		done;
	done;

};

# Проверяем отработку на невалидных значениях (с проверкой на валидность значений)
# В данном тесте мы сравниваем результат выполнения с эталонным (тестирование стека функций initialization - proverka - tip_treyg)
unit_test_f() {

	# Удаляем файл с результатами предыдущего выполнения
	rm $currd/tmp/result_f.lst
	# Начинаем цикл построчного чтения
	cat $currd/var/init_f.lst | while read line
	do
	# Згоняем параметры из файла в цикл
	for param in $line
		do
			# Парсим строку
			a=`printf "$param" | awk -F ";" {'print $1'}`;
			b=`printf "$param" | awk -F ";" {'print $2'}`;
			c=`printf "$param" | awk -F ";" {'print $3'}`;
			tip=`printf "$param" | awk -F ";" {'print $4'}`;
			# Запускаем проверку с выводом статуса выполнения ( успешно \ неуспешно )
			prover=`proverka $a $b $c && echo "SUCCESS" || echo "FAIL"`;
			# Убираем из результата выполнения тип треугольника
			result=`printf "$prover" | sed "s/$tip//"`;

			# Запускаем проверку с выводом в результирующий файл
			if [ "$result" != "SUCCESS" ]; then
				printf "$param;FAIL" >> $currd/tmp/result_f.lst;
			else
				printf "$param;SUCCESS" >> $currd/tmp/result_f.lst;
			fi;
		done;
	done;

};

# Функция инициализации работы скрипта
initialization() {

	# Режим тестирования на валидных значениях (напрямую в функцию расчета)
	if [ "$1" = "--test_sf" ]; then
		unit_test_sf;
	# Режим тестирования на разнотипных значениях (проверка на валидность значений)
	elif [ "$1" = "--test_f" ]; then
		unit_test_f;
	# Вывод справочной иинформации
	elif [ "$1" = "--help" ]; then
	printf "triangle: triangle [-LP]
Использование: triangle [ЗНАЧЕНИЕ ЗНАЧЕНИЕ ЗНАЧЕНИЕ]
       или:    triangle [АРГУМЕНТ]
Скрипт расчета типа треугольника.\n
Необязательные аргументы:
    --test_sf - Режим тестирования на валидных значениях (напрямую в функцию расчета)
    --test_f  - Режим тестирования на разнотипных значениях (проверка на валидность значений)\n";
	else
		read a b c;
		# Берем на вход значения треугольника
		#a=$1; b=$2; c=$3;
		proverka $a $b $c;
	fi;

};

# Запускем инициализацию
initialization $1 $2 $3 $4;
