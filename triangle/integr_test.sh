#!/bin/sh

# Начинаем цикл построчного чтения
cd triangle;
cat var/init_f.lst | while read line
do
	# Згоняем параметры из файла в цикл
	for param in $line
	do
		# Парсим строку
		a=`echo "$param" | awk -F ";" {'print $1'}`;
		b=`echo "$param" | awk -F ";" {'print $2'}`;
		c=`echo "$param" | awk -F ";" {'print $3'}`;
		tip=`echo "$param" | awk -F ";" {'print $4'}`;
		# Запускаем проверку с выводом статуса выполнения основного скрипта ( успешно \ неуспешно )
		prover="$(echo $a $b $c | bash triangle.sh)"
		status=`echo "$?"`;
		echo "$prover. Статус выполнения - $status"

		# Второй вариант
		if [ "$prover" = "$tip" ]; then
			echo "$param;SUCCESS";
		else
			echo "$param;FAIL";
		fi;
		echo -e "\n";
		sleep 1;
	done;
done;

