#!/bin/bash

# Задаем текущую дииректорию
currd=`printf $PWD`;

# Начинаем цикл построчного чтения
cat $currd/var/integr.lst | while read line
do
	# Згоняем параметры из файла в цикл
	for param in $line
	do
		param1=`echo "$param" | tr '_' ' '`;
		# Парсим строку
		a=`echo "$param1" | awk -F ";" {'print $1'}`;
		b=`echo "$param1" | awk -F ";" {'print $2'}`;
		# Запускаем проверку с выводом статуса выполнения основного скрипта ( успешно \ неуспешно )
		prover="$(echo "$a" | ./time.sh)"
		status=`echo "$?"`;
		echo "$prover. Статус выполнения - $status"

		# Второй вариант

		if [ "$prover" = "$b" ]; then
			echo -e "$param1;SUCCESS";
		else
			echo -e "$param1;FAIL";
		fi;
		sleep 1;
	done;
done;

