#!/bin/sh

# Задаем текущую директорию
currd=`printf $PWD`;

# Функция выдачи результата
print_result() {

	result=`cat $currd/var/list_time.lst | grep -P "$currh" | awk -F ';' {'print $2'}`;
	echo "$result";

};

# Функции unit-тестов

negative_number() {

        # Блок проверки на отрицательные \ нецелые число
        chislo=`echo "$currh" | tr -d '[0-9]'`;
        if [ ! -z "$chislo" ]; then
            printf "Что-то пошло не так" >&2;
            exit 1;
        fi;
        null_result;

};

null_result() {

	# Блок детекта переменной со значением null
	# Если переменная пустая, аварийно завершаем приложение
	result=`cat $currd/var/list_time.lst | grep -P "$currh" | tr -d '\;'`;
	if [ -n "$result" ]; then
		bad_count;
	else
		echo "Что-то пошло не так" >&2;
		exit 1;
	fi;

};

bad_count() {

	# Допускаем только числа, в которых есть два символа
	count=`echo $currh | wc -c`;
	if [ $count -eq 3 ]; then
		bad_result;
	else
                echo "Что-то пошло не так" >&2;
                exit 1;
	fi;

};

bad_result() {

	# Блок проверки недопустимых значений времени
	prov=`cat $currd/var/list_time.lst | grep -P "$currh" | awk -F ';' {'print $1'}`;
	# Если такого значения часа нет в словаре - аварийно завершаем приложение
	if [ $prov -eq $currh ]; then
        	print_result;
	else
        	echo "Что-то пошло не так" >&2;
       		exit 1;
	fi;
};

# Функция инициализации работы скрипта
initialization() {

	read currh;
	negative_number $currh;

};

initialization;

